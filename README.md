# wf product card

```js
<ProductCard
  product={product}
  initialValues={{
    count: 4,
    // maxCount: 10,
  }}
>
  {({ count, reset, increasyBy, isMaxCountReached, maxCount }) => {
    return (
      <>
        <ProductCard.Image />
        <ProductCard.Title />
        <ProductCard.Buttons />
      </>
    );
  }}
</ProductCard>
```
