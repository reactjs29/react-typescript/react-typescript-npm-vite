// import { ReactElement } from 'react';
import { PropCardButtons } from '../components/ProductButtons';
import { Props as ProductCardProps } from '../components/ProductCard';
import { ProductCardImage } from '../components/ProductImage';
import { ProductCardTitle } from '../components/ProductTitle';

// export interface ProductCardProps {
//   product: Product;
//   children?: ReactElement | ReactElement[];
// }

export interface Product {
  id: string;
  title: string;
  img?: string;
}

export interface ProductContextProps {
  counter: number;
  increasyBy: (value: number) => void;
  product: Product;
  maxCounter?: number;
}

export interface ProductCardHOCProps {
  ({ children, product }: ProductCardProps): JSX.Element;
  Title: (Props: ProductCardTitle) => JSX.Element;
  Image: (Props: ProductCardImage) => JSX.Element;
  Buttons: (Props: PropCardButtons) => JSX.Element;
}

export interface onChangesArgs {
  product: Product;
  count: number;
}

export interface ProductInCard extends Product {
  count: number;
}

//
export interface InitialValues {
  count?: number;
  maxCount?: number;
}

//
export interface ProductCardHandlers {
  count: number;
  isMaxCountReached: boolean;
  maxCount?: number;
  product: Product;
  increasyBy: (value: number) => void;
  reset: () => void;
}
