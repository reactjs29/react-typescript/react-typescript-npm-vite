import React, { useContext } from 'react';
import noImage from '../assets/no-image.jpg';
import styles from '../styles/styles.module.css';
import { ProductContext } from './ProductCard';

export interface ProductCardImage {
  img?: string;
  className?: string;
  style?: React.CSSProperties;
}

const ProductImage = ({ img, className, style }: ProductCardImage) => {
  const { product } = useContext(ProductContext);
  let imgToShow: string;

  if (img) {
    imgToShow = img;
  } else if (product.img) {
    imgToShow = product.img;
  } else {
    imgToShow = noImage;
  }

  return (
    <img
      className={`${styles.productImg} ${className}`}
      style={style}
      src={imgToShow}
      alt='coffee-mug'
    />
  );
};

export { ProductImage };
