import React, { useCallback, useContext } from 'react';
import styles from '../styles/styles.module.css';
import { ProductContext } from './ProductCard';

export interface PropCardButtons {
  className?: string;
  style?: React.CSSProperties;
}

const ProductButtons = ({ className, style }: PropCardButtons) => {
  const { increasyBy, counter, maxCounter } = useContext(ProductContext);

  const isMaxReached = useCallback(
    () => !!maxCounter && counter === maxCounter,
    [maxCounter, counter]
  );
  // console.log(isMaxReached());
  // console.log({ maxCounter, counter });

  return (
    <div className={`${styles.buttonsContainer} ${className}`} style={style}>
      <button className={styles.buttonMinus} onClick={() => increasyBy(-1)}>
        -
      </button>
      <div className={styles.countLabel}>{counter}</div>
      {/* {!isMaxReached() && ( */}
      <button
        className={`${styles.buttonAdd} ${isMaxReached() && styles.disabled}} `}
        onClick={() => increasyBy(1)}
      >
        +
      </button>
      {/* )} */}
    </div>
  );
};

export { ProductButtons };
