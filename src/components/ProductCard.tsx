import React, { createContext } from 'react';
import useProduct from '../hooks/useProduct';

import styles from '../styles/styles.module.css';
import { ProductCardHandlers } from '../interfaces/interfaces';

import {
  InitialValues,
  onChangesArgs,
  Product,
  ProductContextProps,
} from '../interfaces/interfaces';

export const ProductContext = createContext({} as ProductContextProps);
const { Provider } = ProductContext;

export interface Props {
  product: Product;
  // children?: ReactElement | ReactElement[];
  children: (args: ProductCardHandlers) => JSX.Element;
  className?: string;
  style?: React.CSSProperties;
  onChange?: (args: onChangesArgs) => void;
  value?: number;
  initialValues?: InitialValues;
}

const ProductCard = ({
  children,
  product,
  className,
  style,
  onChange,
  value,
  initialValues,
}: Props) => {
  const {
    counter,
    increasyBy,
    maxCounter,
    isMaxCountReached,
    reset,
  } = useProduct({
    onChange,
    product,
    value,
    initialValues,
  });

  return (
    <Provider
      value={{
        counter,
        increasyBy,
        product,
        maxCounter,
      }}
    >
      <div className={`${styles.productCard} ${className}`} style={style}>
        {children({
          count: counter,
          isMaxCountReached,
          maxCount: initialValues?.maxCount,
          product,
          increasyBy,
          reset,
        })}
      </div>
    </Provider>
  );
};

export { ProductCard };
