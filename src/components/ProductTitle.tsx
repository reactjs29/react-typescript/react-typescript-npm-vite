import React, { useContext } from 'react';
import styles from '../styles/styles.module.css';
import { ProductContext } from './ProductCard';

export interface ProductCardTitle {
  title?: string;
  className?: string;
  style?: React.CSSProperties;
}

const ProductTitle = ({ title, className, style }: ProductCardTitle) => {
  const { product } = useContext(ProductContext);

  return (
    <span className={`${styles.productDescription} ${className}`} style={style}>
      {title ? title : product.title}
    </span>
  );
};

export { ProductTitle };
